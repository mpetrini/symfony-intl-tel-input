<?php

namespace Mpetrini\IntlTelTypeBundle\Tests\DependencyInjection;

use Mpetrini\IntlTelTypeBundle\IntlTelTypeBundle;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ConfigurationTest extends TestCase
{
    private function getContainer(array $configs = array()): ContainerBuilder
    {
        $container = new ContainerBuilder();

        $extension = (new IntlTelTypeBundle())->getContainerExtension();
        $extension->load($configs, $container);

        return $container;
    }

    public function testConfig(): void
    {
        $container = $this->getContainer(array(
            array(
                'default_country' => 'fr_FR',
            ),
        ));

        //$this->assertEquals('fr_FR', $container->getExtensions());
    }
}
