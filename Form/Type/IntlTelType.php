<?php

namespace Mpetrini\IntlTelTypeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Intl type.
 *
 * @author Mathieu Petrini <mathieupetrini@gmail.com>
 */
class IntlTelType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }

    public function getName()
    {
        return 'intl_tel_type';
    }
}
