intl-tel-input-bundle
=====================

This bundle integrate [International Telephone Input](https://github.com/Bluefieldscom/intl-tel-input). into symfony4 as a form type.

![alt tag](https://raw.githubusercontent.com/SpikeO/intl-tel-input-bundle/master/screenshot.png)


## Demo and Examples
You can view a live demo and some examples of how to use the various options here: http://jackocnr.com/intl-tel-input.html.



Installation
============

## Using Composer

Add the intl-tel-input-bundle in your `composer.json` file:

``` bash
$ php composer.phar require mpetrini/intl-tel-input-bundle
```

Composer will install the bundle in your project's `vendor` directory.
